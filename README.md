How do you pursue a Career in Residential School Teaching?

Career in Residential School Teaching


Residential School Teaching

A Career in Residential School Teaching has always intrigued me. For me understanding Why I want to choose a Career in Residential School Teaching is phenomenally more important than figuring out How to get into Residential School Teaching. While I was searching for reliable information about a Career in Residential School Teaching, I came across this amazing page: https://www.lifepage.in/careers/residential-school-teaching

Priyanka Bhattacharyya's perspective!
Priyanka Bhattacharyya has worked in Residential School Teaching for 15 years & 2 months. Priyanka Bhattacharyya has worked in Residential School Teaching as Head of Department in The Doon School. In Priyanka Bhattacharyya's own words, this is how Priyanka Bhattacharyya got into Residential School Teaching: "After completing my education I joined Doon School as a Residential Teacher. I have been working at Doon School since 2005." Priyanka Bhattacharyya has a profile in Resume 2.0 format on: https://www.lifepage.in/page/priyankabhattacharyya

Career Video on Residential School Teaching
In a video, Priyanka Bhattacharyya has talked about various aspects of a Career in Residential School Teaching. Priyanka Bhattacharyya started by explaining Residential School Teaching as: "Residential School Teaching is a unique mix of academic teaching and pastoral care." The video was an engaging disposition.


We all know that only 10% of what is taught in Residential School Teaching is actually used in real life. The education section of the video clearly explained what is the 10% needed in Residential School Teaching. Priyanka Bhattacharyya touches upon these in the Education section of the Video:

    Pedagogy
    Content Knowledge
    Liberal Arts
    Psychology

Skills are the most important factor determining success in a particular Career.
Priyanka Bhattacharyya then explains why these Skills are essential for a Career in Residential School Teaching:

    ICT Skills
    Research Methods
    Class Management Skills
    People Skills
    Resource Management
    Communication Skills

It is important to get an understanding of the Positives of this Career.
Priyanka Bhattacharyya believes that the following are some of the Positives of a Career in Residential School Teaching:

    Job Satisfaction
    Continuous Learning
    Great growth possibilities
    Competitive Salaries

There are a few Challenges in this Career which one needs to be cognizant of.
And, Priyanka Bhattacharyya believes that one needs to prepare for following Challenges of a Career in Residential School Teaching:

    Work Life Balance
    High Pressure
    Moral Responsibility
    24 * 7 Presence


In the final section of the video Priyanka Bhattacharyya talks about How a day goes in a Career in Residential School Teaching. This video is by far the best video on a Career in Residential School Teaching, that I have ever come across. To see the full Talk, one needs to install the LifePage Career Talks App. Here is a direct deep link of the Video: https://lifepage.app.link/20161223-0001
Career Counseling 2.0
This video on a Career in Residential School Teaching was an eye opener and it got me very interested to learn more about the LifePage platform. LifePage allows you to explore thousands of Career Options. LifePage is the world’s most evolved Career Platform. You can use LifePage to find your Career Objective. LifePage also offers the most comprehensive Career Planning process. You can use LifePage to explore more than a thousand Career Options. LifePage has the most exhaustive Career List. It is truly Career Counseling 2.0 LifePage has made a science of Career Counseling. Its awareness focussed approach is much more practical and action-oriented rather than absolutely any other Career platform. I suggest you learn more about them on: https://www.lifepage.in

Similar Career List on LifePage
I continued with my research on LifePage and thoroughly studied these links to gain more perspective:

    Career in Teaching Primary Kids
    [Teacher | Various Schools]
    https://www.lifepage.in/careers/teaching-primary-kids


    Career in Primary Teaching
    [Teacher | Scholars Home]
    https://www.lifepage.in/careers/primary-teaching


    Career in Teaching
    [Teacher | Government Girls Junior High School, Mehuwala]
    https://www.lifepage.in/careers/teaching-2


    Career in Teaching
    [Teacher | Teach For India]
    https://www.lifepage.in/careers/teaching-3


    Career in Teaching Geography
    [Associate Professor | Government PG College]
    https://www.lifepage.in/careers/teaching-geography


    Career in Teaching Small Children
    [Teacher | Olive International School]
    https://www.lifepage.in/careers/teaching-small-children


    Career in School Teaching
    [School Teacher | St Josephs Academy]
    https://www.lifepage.in/careers/school-teaching


    Career in Teaching
    [Fellow | Teach for India]
    https://www.lifepage.in/careers/teaching-4


    Career in Teaching Calligraphy
    [Founder | Shaddy Creative Classes]
    https://www.lifepage.in/careers/teaching-calligraphy


    Career in Teaching Small Children
    [Teacher | Instituto Vocacional Concepcion]
    https://www.lifepage.in/careers/teaching-small-children-1


    Career in Teaching Economics
    [Teacher | Zest Education Center]
    https://www.lifepage.in/careers/teaching-economics


    Career in Teaching Small Children
    [Principal | John Martyn Memorial School]
    https://www.lifepage.in/careers/teaching-small-children-2


    Career in Teaching English
    [Primary Teacher | St. Giri Public School]
    https://www.lifepage.in/careers/teaching-english-5


    Career in Teaching and Learning
    [Fellow | Teach for India]
    https://www.lifepage.in/careers/teaching-and-learning


    Career in After School Education
    [Co-founder | Ananda Vatika Green Gurukulam]
    https://www.lifepage.in/careers/after-school-education


    Career in Teaching Economics
    [Post Graduate Teacher | Kothari International School]
    https://www.lifepage.in/careers/teaching-economics-1


    Career in Teacher Training
    [Teacher Trainer | Various Assignments]
    https://www.lifepage.in/careers/teacher-training


    Career in Teaching Political Science
    [Assistant Professor | JNU]
    https://www.lifepage.in/careers/teaching-political-science


    Career in Teaching Social Sciences
    [Assistant Professor | Himachal Pradesh University, Shimla]
    https://www.lifepage.in/careers/teaching-social-sciences


    Career in Teaching Social Science
    [Teacher & Coordinator | St Giri Public School & Humanities Hub]
    https://www.lifepage.in/careers/teaching-social-science


    Career in Primary Teaching
    [Primary Teacher | Frank Anthony Public School]
    https://www.lifepage.in/careers/primary-teaching-1


    Career in School Teaching
    [Teacher | INTACH]
    https://www.lifepage.in/careers/school-teaching-1


    Career in Clinical Psychology
    [Assistant Professor Grad III | Amity University]
    https://www.lifepage.in/careers/clinical-psychology-3


    Career in Teaching History
    [Trained Graduate Teacher | Summer Valley School]
    https://www.lifepage.in/careers/teaching-history


    Career in Teaching Geography
    [Trained Graduate Teacher | Summer Valley School]
    https://www.lifepage.in/careers/teaching-geography-1


    Career in Teaching History
    [Teacher | Doon International School]
    https://www.lifepage.in/careers/teaching-history-1


    Career in Teaching History
    [Assistant Professor | UPES]
    https://www.lifepage.in/careers/teaching-history-2


    Career in Teaching Social Studies
    [Teacher | Welham Boys School]
    https://www.lifepage.in/careers/teaching-social-studies


    Career in Teaching Political Science
    [PGT Political Science Teacher | The Mother's International School]
    https://www.lifepage.in/careers/teaching-political-science-1


    Career in Teaching French
    [Professor & Language Trainer | Alliance Francaise de Delhi (AFD)]
    https://www.lifepage.in/careers/teaching-french-1



Information about other Career Options
I also looked out for more information about other Career options and found these great articles:

    Career in Entrepreneurship
    https://paste.feed-the-beast.com/view/998c945f

    Career in Cabin Crew Flying
    https://www.bigpage.site/2019/12/how-do-i-start-career-in-cabin-crew.html

    Career in Entrepreneurship
    https://www.entrepreneurshipguide.site/2019/12/is-entrepreneurship-good-career-option_31.html

    Career in Research in Data Science
    https://www.careeradvice.tech/2019/12/what-is-career-in-research-in-data.html

    Career in Chartered Accountancy
    https://www.classifiedads.com/construction_work_jobs/97fbm2nx6244x

    Career in Modeling
    https://www.modelling.website/2019/12/how-do-you-pursue-career-in-modeling_30.html

    Career in Teaching Anatomy
    https://www.careeradvisor.online/2019/12/how-do-you-pursue-career-in-teaching.html

    Career in Program Management
    https://rentry.co/6n5su

    Career in Creative Writing
    https://www.careeradvice.online/2019/12/is-creative-writing-good-career-option.html

    Career in Creative Designing
    https://www.graphic-designing.space/2019/12/how-do-you-pursue-career-in-creative.html

    Career in Entrepreneurship
    https://www.entrepreneurshipguide.site/2019/12/what-is-career-in-entrepreneurship.html

    Career in Marine Engineering
    https://www.merchant-navy.space/2019/12/is-marine-engineering-good-career-option.html

    Career in Folk Singing
    https://www.careeradvisor.online/2019/12/what-is-career-in-folk-singing.html

    Career in Food and Beverage Operations
    https://www.hotel-management.space/2019/12/how-do-i-start-career-in-food-and.html

    Career in Mechanical Engineering
    https://www.mechanical-engineering.space/2019/12/how-do-i-start-career-in-mechanical_31.html

    Career in Design Engineering
    https://www.civil-engineering.space/2019/12/how-do-i-start-career-in-design.html

    Career in Psychiatry
    https://www.physiotherapy.tech/2019/12/what-is-career-in-psychiatry.html

    Career in Makeup Artistry
    https://www.careeradvice.tech/2019/12/what-is-career-in-makeup-artistry.html

    Career in Graphic Designing
    https://www.graphic-designing.space/2019/12/how-do-you-pursue-career-in-graphic.html

    Career in Indian Army
    https://www.careeradvisor.online/2019/12/what-is-career-in-indian-army.html

    Career in Sports Medicine & Pain Management
    https://www.careeradviceonline.online/2019/12/how-do-i-start-career-in-sports.html

    Career in Petroleum Dealership
    https://ghostbin.co/paste/hg45c

    Career in Front Office Operations
    https://www.hotel-management.space/2019/12/how-do-you-pursue-career-in-front.html

    Career in Garment Designing
    https://www.fashion-designing.space/2019/12/is-garment-designing-good-career-option.html

    Career in Communication Consulting
    https://www.careerchatonline.online/2019/12/how-do-i-start-career-in-communication.html




Interesting Career Articles
Education System in India 	Education System in India
LifePage App – choose a Career in 3 steps 	LifePage App – choose a Career in 3 steps
Most important aspect of choosing a Career 	Most important aspect of choosing a Career
3 Fundamental problems with Career Guidance 	3 Fundamental problems with Career Guidance
How and Why of choosing a Career 	How and Why of choosing a Career




Leading Design company in India is based in Dehradun, it specializes in Architecture, Interior, Landscape and Planning services. Have a look at http://www.aka.net.in

And, in case you are interested in Comment Blogging for SEO then you should definitely visit this incredible resource: https://www.mechanical-engineering.space/2019/12/126-un-moderated-blog-posts-for-quick.html

